from django.urls import path

from .views import IndexView, logout_view, basic_data_view, audience_gender_age_view, audience_country_view, media_view, \
    comments_view, impressions_view, reach_view

app_name = 'main'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('logout', logout_view, name='logout'),
    path('discovery/basic/<int:ig_business_id>/<str:ig_username>', basic_data_view, name='basic_data'),
    path('discovery/audience_gender_age/<int:ig_business_id>', audience_gender_age_view, name='audience_gender_age'),
    path('discovery/audience_country/<int:ig_business_id>', audience_country_view, name='audience_country'),
    path('discovery/media/<int:ig_business_id>', media_view, name='media'),
    path('discovery/comments/<int:ig_business_id>', comments_view, name='comments'),
    path('discovery/impressions/<int:ig_business_id>', impressions_view, name='impressions'),
    path('discovery/reach/<int:ig_business_id>', reach_view, name='reach'),
]