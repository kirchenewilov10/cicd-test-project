import csv
from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import TemplateView

from social_django.models import UserSocialAuth
from pyfacebook.api import IgProApi


class IndexView(TemplateView):
    template_name = "main/index.html"

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated:
            social_auth = UserSocialAuth.objects.filter(user=self.request.user, provider='facebook').first()
            if social_auth:
                return social_auth.extra_data


def logout_view(request):
    logout(request)
    return redirect(reverse('main:index'))


@login_required
def basic_data_view(request, ig_business_id, ig_username):
    social_auth = UserSocialAuth.objects.filter(user=request.user, provider='facebook').first()
    if social_auth:
        extra_data = social_auth.extra_data
        access_token = extra_data.get('access_token')
        api = IgProApi(app_id=settings.SOCIAL_AUTH_FACEBOOK_KEY, app_secret=settings.SOCIAL_AUTH_FACEBOOK_SECRET,
                       long_term_token=access_token, instagram_business_id=ig_business_id)
        api_response = api.discovery_user(
            ig_username,
            fields='biography, id, ig_id, followers_count, follows_count, media_count, name, profile_picture_url, username, website'
        )
        data = api_response.as_dict()

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="{ig_username} - basic data.csv"'

        writer = csv.writer(response, delimiter=';')
        writer.writerow(data.keys())
        writer.writerow(data.values())

        return response


@login_required()
def audience_gender_age_view(request, ig_business_id):
    social_auth = UserSocialAuth.objects.filter(user=request.user, provider='facebook').first()
    if social_auth:
        extra_data = social_auth.extra_data
        access_token = extra_data.get('access_token')
        api = IgProApi(app_id=settings.SOCIAL_AUTH_FACEBOOK_KEY, app_secret=settings.SOCIAL_AUTH_FACEBOOK_SECRET,
                       long_term_token=access_token, instagram_business_id=ig_business_id)
        api_response = api.get_user_insights(ig_business_id, period="lifetime", metrics=["audience_gender_age"])
        data = [item.as_dict() for item in api_response]
        data_sorted = sorted(data[0]['values'][0]['value'].items(), key=lambda x: x[1])

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="{ig_business_id} - audience_gender_age.csv"'

        writer = csv.writer(response, delimiter=';')
        writer.writerow(['Gender/age group', 'Count'])
        for key, value in data_sorted:
            writer.writerow([key, value])

        return response


@login_required()
def audience_country_view(request, ig_business_id):
    social_auth = UserSocialAuth.objects.filter(user=request.user, provider='facebook').first()
    if social_auth:
        extra_data = social_auth.extra_data
        access_token = extra_data.get('access_token')
        api = IgProApi(app_id=settings.SOCIAL_AUTH_FACEBOOK_KEY, app_secret=settings.SOCIAL_AUTH_FACEBOOK_SECRET,
                       long_term_token=access_token, instagram_business_id=ig_business_id)
        api_response = api.get_user_insights(ig_business_id, period="lifetime", metrics=["audience_country"])
        data = [item.as_dict() for item in api_response]
        data_sorted = sorted(data[0]['values'][0]['value'].items(), key=lambda x: x[1])

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="{ig_business_id} - audience_country.csv"'

        writer = csv.writer(response, delimiter=';')
        writer.writerow(['Country', 'Count'])
        for key, value in data_sorted:
            writer.writerow([key, value])

        return response


@login_required()
def media_view(request, ig_business_id):
    social_auth = UserSocialAuth.objects.filter(user=request.user, provider='facebook').first()
    if social_auth:
        extra_data = social_auth.extra_data
        access_token = extra_data.get('access_token')
        api = IgProApi(app_id=settings.SOCIAL_AUTH_FACEBOOK_KEY, app_secret=settings.SOCIAL_AUTH_FACEBOOK_SECRET,
                       long_term_token=access_token, instagram_business_id=ig_business_id)
        api_response = api.get_user_medias(
            ig_business_id,
            fields='caption, comments_count, id, ig_id, like_count, media_type, media_url, permalink, shortcode, '
                   'thumbnail_url, timestamp, insights.metric(reach, impressions)',
            count=None,
            limit=50,
            return_json=True)

        posts_list = []
        for post in api_response:
            reach = list(filter(lambda x: x['name'] == 'reach', post['insights']['data']))
            post['reach'] = reach[0]['values'][0]['value']

            impressions = list(filter(lambda x: x['name'] == 'impressions', post['insights']['data']))
            post['impressions'] = impressions[0]['values'][0]['value']

            del post['insights']
            posts_list.append(post)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="{ig_business_id} - media.csv"'

        writer = csv.writer(response, delimiter=';')
        header = ['comments_count', 'ig_id', 'caption', 'media_type', 'media_url', 'owner', 'permalink', 'shortcode',
                  'thumbnail_url', 'timestamp', 'username', 'is_comment_enabled', 'comments_count',
                  'like_count', 'reach', 'impressions']
        writer.writerow(header)
        for item in posts_list:
            writer.writerow([item.get(field) for field in header])

        return response


@login_required()
def comments_view(request, ig_business_id):
    social_auth = UserSocialAuth.objects.filter(user=request.user, provider='facebook').first()
    if social_auth:
        extra_data = social_auth.extra_data
        access_token = extra_data.get('access_token')
        api = IgProApi(app_id=settings.SOCIAL_AUTH_FACEBOOK_KEY, app_secret=settings.SOCIAL_AUTH_FACEBOOK_SECRET,
                       long_term_token=access_token, instagram_business_id=ig_business_id)
        api_response = api.get_user_medias(
            ig_business_id,
            fields='permalink, comments.fields(hidden, id, like_count, text, timestamp, username)',
            count=None,
            limit=50)

        comments_list_flat = []
        for post in api_response:
            if post.comments:
                comments_for_post = [comment.as_dict() for comment in post.comments]
                for comment in comments_for_post:
                    comment['post_id'] = post.id
                    comment['post_permalink'] = post.permalink
                    comments_list_flat.append(comment)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="{ig_business_id} - comments.csv"'

        writer = csv.writer(response, delimiter=';')
        writer.writerow(
            ['post_id', 'post_url', 'id', 'hidden', 'like_count', 'text', 'timestamp', 'username'])
        for comment in comments_list_flat:
            writer.writerow(
                [comment['post_id'], comment['post_permalink'], comment['id'], comment['hidden'], comment['like_count'],
                 comment['text'], comment['timestamp'], comment['username']])

        return response


@login_required()
def impressions_view(request, ig_business_id):
    social_auth = UserSocialAuth.objects.filter(user=request.user, provider='facebook').first()
    if social_auth:
        extra_data = social_auth.extra_data
        access_token = extra_data.get('access_token')
        api = IgProApi(app_id=settings.SOCIAL_AUTH_FACEBOOK_KEY, app_secret=settings.SOCIAL_AUTH_FACEBOOK_SECRET,
                       long_term_token=access_token, instagram_business_id=ig_business_id)
        now = datetime.now()
        month_earlier = now - timedelta(days=29)
        since = int(month_earlier.timestamp())

        api_response = api.get_user_insights(
            ig_business_id, metrics='impressions', period='day', since=since)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="{ig_business_id} - impressions.csv"'

        writer = csv.writer(response, delimiter=';')
        writer.writerow(['date', 'impressions'])
        for item in api_response[0].values:
            writer.writerow([item.end_time, item.value])

        return response


@login_required()
def reach_view(request, ig_business_id):
    social_auth = UserSocialAuth.objects.filter(user=request.user, provider='facebook').first()
    if social_auth:
        extra_data = social_auth.extra_data
        access_token = extra_data.get('access_token')
        api = IgProApi(app_id=settings.SOCIAL_AUTH_FACEBOOK_KEY, app_secret=settings.SOCIAL_AUTH_FACEBOOK_SECRET,
                       long_term_token=access_token, instagram_business_id=ig_business_id)
        now = datetime.now()
        month_earlier = now - timedelta(days=29)
        since = int(month_earlier.timestamp())

        api_response = api.get_user_insights(
            ig_business_id, metrics='reach', period='day', since=since)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="{ig_business_id} - reach.csv"'

        writer = csv.writer(response, delimiter=';')
        writer.writerow(['date', 'reach'])
        for item in api_response[0].values:
            writer.writerow([item.end_time, item.value])

        return response
